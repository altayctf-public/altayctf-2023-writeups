import re
from secrets import token_hex
from typing import TypedDict

import aiohttp
from aiohttp import ClientResponse, ClientSession
from faker.providers import lorem, profile, user_agent
from faker.providers.misc import Provider as MiscProvider

from lib.checkers import BaseServiceChecker, try_action

CSRF_TOKEN_PATTERN = re.compile(r'<input type="hidden" name="_token" value="([^"]+)">')


class Credentials(TypedDict):
    username: str
    password: str
    dictionary_id: int
    flag: str


class Checker(BaseServiceChecker):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fake_en.add_provider(user_agent)
        self.fake_en.add_provider(lorem)
        self.fake_en.add_provider(profile)
        self.fake_en.add_provider(MiscProvider)
        self.fake_ru.add_provider(MiscProvider)
        self.credentials: Credentials
        self.old_credentials: Credentials

    async def check(self):
        cookie_jar = aiohttp.CookieJar(unsafe=True)
        async with aiohttp.ClientSession(
            loop=self.event_loop,
            cookie_jar=cookie_jar,
            headers={'User-Agent': self.fake_en.user_agent()},
        ) as client:
            if not await self.fetch_main_page(client):
                return
            
            self.logger.debug("Main page available")
            self.status += 5

            if set(["flag", "username", "password", "dictionary_id"]) <= set(self.old_credentials.keys()):
                await self._check_previous_flag()
            else:
                self.messages.append("Нет данных о предыдущем флаге")

            if not await self.check_search(client):
                return

            self.logger.debug("Search available")
            self.status += 15

            fake_profile = self.fake_en.profile()
            fake_profile["username"] += token_hex(4)
            user_password = self.fake_en.password(length=10, special_chars=False)
            self.credentials["username"] = fake_profile["username"]
            self.credentials["password"] = user_password
            if not await self.register(
                client,
                name=fake_profile["name"],
                email=fake_profile["mail"],
                password=user_password,
                username=fake_profile["username"],
            ):
                return

            self.logger.debug("Registration available")
            self.status += 5

            if not await self.login(
                client,
                username=fake_profile["username"],
                password=user_password,
            ):
                return
            
            self.logger.debug("Login available")
            self.status += 5

            if not (dictionary_id := await self.create_dictionary(
                sess=client,
                description=self.fake_en.paragraph(nb_sentences=3),
                is_private="1",
                name=self.fake_en.word() + f"_{token_hex(8)}",
            )):
                return

            self.credentials["dictionary_id"] = dictionary_id
            self.logger.debug("Dictionary creation available")
            self.status += 10

            for i in range(2):
                if not await self.add_translation(
                    client,
                    dictionary_id=dictionary_id,
                    original_word=self.fake_en.word() + f"_{token_hex(12)}",
                    translation=self.fake_ru.word() + f"_{token_hex(12)}",
                ):
                    return
                
                self.logger.debug(f"Created translation #{i+1}")
                self.status += 5
            

            flag = self.generate_flag()
            if not await self.add_translation(
                client,
                dictionary_id=dictionary_id,
                original_word=self.fake_en.word() + f"_{token_hex(12)}",
                translation=flag,
            ):
                return
            
            self.logger.debug(f"Saved flag {flag}")
            self.credentials["flag"] = flag
            self.status += 10

            if not (flags := await self.extract_flags_from_dictionary(client, dictionary_id)):
                return
            
            self.logger.debug(f"Extracted flags: {flags}")
            self.status += 10

        return

    async def _check_previous_flag(self) -> bool:
        cookie_jar = aiohttp.CookieJar(unsafe=True)
        async with aiohttp.ClientSession(
            loop=self.event_loop,
            cookie_jar=cookie_jar,
            headers={'User-Agent': self.fake_en.user_agent()},
        ) as client:
            if not await self.login(
                client,
                username=self.old_credentials["username"],
                password=self.old_credentials["password"],
            ):
                return
            
            self.logger.debug("Login with old creds available")
            self.status += 10

            if not (flags := await self.extract_flags_from_dictionary(client, self.old_credentials["dictionary_id"])):
                return
            
            self.logger.debug(f"Old dictionary accessible")
            if self.old_credentials["flag"] not in flags:
                return
            
            self.logger.debug(f"Old flag accessible")
            self.flag_retrieved = True
            self.status += 20

    async def fetch_main_page(self, sess: ClientSession) -> None:
        with try_action(self, "Недоступна индексная страница"):
            async with sess.request('GET', f'http://{self.target}/') as response:
                assert response.status == 200
                return aiohttp.ClientResponse

    async def check_search(self, sess: ClientSession) -> bool:
        with try_action(self, "Не работает функция поиска"):
            async with sess.get(f'http://{self.target}/search', params={
                "text": "test",
                "lang_direction": "0",
            }) as response:
                assert response.status != 503
                return True

    async def login(self, sess: ClientSession, username: str, password: str) -> bool:
        if not (csrf_token := await self._fetch_csrf_token(sess, f'http://{self.target}/login')):
            return
        
        with try_action(self, f"Не удалось войти в аккаунт {username}"):
            async with sess.post(f'http://{self.target}/login', data={
                "username": username,
                "password": password,
                "_token": csrf_token,
            }) as response:
                assert response.status == 200
                assert response.url.path == '/'
                return True

    async def register(self, sess: ClientSession, name: str, username: str, email: str, password: str) -> bool:
        if not (csrf_token := await self._fetch_csrf_token(sess, f'http://{self.target}/register')):
            return
        
        with try_action(self, "Не удалось зарегистрироваться"):
            async with sess.post(f'http://{self.target}/register', data={
                "name": name,
                "email": email,
                "password": password,
                "username": username,
                "_token": csrf_token,
            }) as response:
                assert response.status == 200
                return True

    async def create_dictionary(self, sess: ClientSession, name: str, description: str, is_private: str) -> int:
        if not (csrf_token := await self._fetch_csrf_token(sess, f'http://{self.target}/profile/dictionaries/create')):
            return
        
        with try_action(self, "Не удалось создать приватный словарь"):
            async with sess.post(f'http://{self.target}/profile/dictionaries/create', data={
                "name": name,
                "lang_direction": "en-ru",
                "description": description,
                "is_private": is_private,
                "_token": csrf_token,
            }) as response:
                assert response.status == 200
                return int(response.url.path.rsplit("/", maxsplit=1)[-1])

    async def add_translation(self, sess: ClientSession, dictionary_id: int, original_word: str, translation: str) -> bool:
        if not (csrf_token := await self._fetch_csrf_token(sess, f'http://{self.target}/profile/dictionaries/{dictionary_id}')):
            return
        
        with try_action(self, f"Не удалось добавить перевод в словарь {dictionary_id}"):
            async with sess.post(f'http://{self.target}/profile/dictionaries/{dictionary_id}/translation', data={
                "original_word": original_word,
                "translation": translation,
                "_token": csrf_token,
            }) as response:
                assert response.status == 200
                return True

    async def extract_flags_from_dictionary(self, sess: ClientSession, dictionary_id: int) -> list[str]:
        with try_action(self, f"Не удалось получить флаги из словаря {dictionary_id}"):
            async with sess.get(f'http://{self.target}/profile/dictionaries/{dictionary_id}') as response:
                assert response.status == 200
                response_text = await response.text()
                return self._extract_flags(response_text)

    async def _fetch_csrf_token(self, sess: ClientSession, url: str) -> str | None:
        with try_action(self, f"Не удалось получить CSRF токен из {url}"):
            if resp := await self._fetch_page(sess, url):
                return await self._extract_csrf_token(await resp.text())

    async def _fetch_page(self, sess: ClientSession, url: str) -> ClientResponse:
        with try_action(self, f"Недоступна страница {url}"):
            async with sess.request('GET', url) as response:
                assert response.status == 200
                await response.read()
                return response

    async def _extract_csrf_token(self, text: str) -> str | None:
        """
        Extract csrf token using regex from response, from format:
        <input type="hidden" name="_token" value="rolg4lYnRZ2haS6FwWo8N9PfBtIScIUbGOeAMJUi">
        """
        with try_action(self, "Не удалось извлечь CSRF токен"):
            match = CSRF_TOKEN_PATTERN.search(text)
            assert match is not None
            return match.group(1)
