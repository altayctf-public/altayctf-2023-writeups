import random
from secrets import token_hex
from typing import Any, TypedDict
import aiohttp
from aiohttp import ClientSession

from lib.checkers import BaseServiceChecker, try_action
from faker.providers import lorem, profile, user_agent
from faker.providers.misc import Provider as MiscProvider
from dataclasses import dataclass


RESULT_OK = "The payment was completed"
RESULT_NO_MONEY = "Insufficient funds"


@dataclass
class UserInfo:
    id: int
    auth: str


class Credentials(TypedDict):
    username: str
    password: str
    flag: str


class Checker(BaseServiceChecker):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fake_en.add_provider(profile)
        self.fake_en.add_provider(user_agent)
        self.fake_en.add_provider(lorem)
        self.fake_en.add_provider(MiscProvider)
        self.credentials: Credentials
        self.old_credentials: Credentials

    async def check(self):
        async with aiohttp.ClientSession(
            loop=self.event_loop,
            cookie_jar=aiohttp.CookieJar(unsafe=True),
            headers={
                "User-Agent": self.fake_en.user_agent(),
                "Content-Type": "application/json",
            },
        ) as client:
            if not isinstance((ads := await self.get_all_ads(client)), list):
                return

            self.logger.debug("Ads page available")
            self.status += 20

            if set(["username", "password", "flag"]) <= set(self.old_credentials.keys()):
                await self.check_previous_flag()
            else:
                self.messages.append("Нет данных о предыдущем флаге")
            
            profile = self.fake_en.profile()
            profile["username"] += token_hex(4)
            password = self.fake_en.password(length=10, special_chars=False)
            flag = self.generate_flag()
            if not await self.register(
                sess=client,
                username=profile["username"],
                password=password,
                secret_question=self.fake_en.sentence(nb_words=3),
                secret_answer=flag,
            ):
                return
            
            self.logger.debug("Registered")
            self.status += 5

            if not (user := await self.login(sess=client, username=profile["username"], password=password)):
                return
            
            self.logger.debug("Logged in")
            self.status += 5

            if not (profile := await self.get_profile(sess=client, user=user)):
                return
            
            with try_action(self, "Не удалось прочитать флаг в профиле после регистрации"):
                assert profile["secretAnsver"] == flag
                self.credentials.update(
                    {
                        "flag": flag,
                        "password": password,
                        "username": profile["username"],
                    }
                )
            
            self.logger.debug("Flag read")
            self.status += 5

            if not await self.create_ad(
                sess=client,
                user=user,
                description=self.fake_en.sentence(nb_words=10),
                price=random.randint(9000, 11000),
                title=self.fake_en.word() + token_hex(16),
            ):
                return
            
            self.status += 5
            if not await self.create_ad(
                sess=client,
                user=user,
                description=self.fake_en.sentence(nb_words=10),
                price=random.randint(50, 500),
                title=self.fake_en.word() + token_hex(16),
            ):
                return

            self.logger.debug("Ads created")
            self.status += 5

            profile_2 = self.fake_en.profile()
            profile_2["username"] += token_hex(4)
            password_2 = self.fake_en.password(length=10, special_chars=False)
            flag = self.generate_flag()
            if not await self.register(
                sess=client,
                username=profile_2["username"],
                password=password_2,
                secret_question=self.fake_en.sentence(nb_words=3),
                secret_answer=flag,
            ):
                return
            
            self.logger.debug("Registered second user")
            self.status += 5

            if not (user_2 := await self.login(sess=client, username=profile_2["username"], password=password_2)):
                return
            
            self.logger.debug("Logged in second user")
            self.status += 5

            if not (ads := await self.get_all_ads(client)):
                return
            
            with try_action(self, "Не удалось спарсить список товаров"):
                available_ad = random.choice([ad for ad in ads if ad["price"] <= 1000 and ad["ownerId"] != user_2.id])
                if not await self.get_ad(sess=client, ad_id=available_ad["id"]):
                    return

            self.logger.debug(f"Ad page with id {available_ad['id']} available")
            self.status += 5

            if not (buy_result := await self.buy_ad(
                sess=client,
                user=user_2,
                ad_id=available_ad['id'],
            )):
                return
            
            with try_action(self, "Не удалось купить объявление"):
                assert buy_result == RESULT_OK
                self.logger.debug("Ad bought")
                self.status += 5
            
            with try_action(self, "Не удалось спарсить список товаров"):
                unavailable_ad = random.choice([ad for ad in ads if ad["price"] > 1000 and ad["ownerId"] != user_2.id])
                if not (buy_result := await self.buy_ad(
                    sess=client,
                    user=user_2,
                    ad_id=unavailable_ad['id'],
                )):
                    return
            
            with try_action(self, "Не удалось купить объявление"):
                assert buy_result == RESULT_NO_MONEY
                self.logger.debug(f"Ad {unavailable_ad['id']} not available (correct)")
                self.status += 5

    async def check_previous_flag(self) -> None:
        cookie_jar = aiohttp.CookieJar(unsafe=True)
        async with aiohttp.ClientSession(
            loop=self.event_loop,
            cookie_jar=cookie_jar,
            headers={'User-Agent': self.fake_en.user_agent()},
        ) as client:
            if not (user_info := await self.login(
                sess=client,
                username=self.old_credentials["username"],
                password=self.old_credentials["password"],
            )):
                return
            
            self.logger.debug("Logged in with old credentials")
            self.status += 10

            if not (profile := await self.get_profile(sess=client, user=user_info)):
                return
            
            with try_action(self, "Не удалось прочитать флаг в профиле старого пользователя"):
                assert profile["secretAnsver"] == self.old_credentials["flag"]
                self.logger.debug("Old flag retrieved")
                self.flag_retrieved = True
                self.status += 20

    async def register(
        self,
        sess: ClientSession,
        username: str,
        password: str,
        secret_question: str,
        secret_answer: str,
    ) -> bool:
        with try_action(self, "Не удалось зарегистрироваться"):
            async with sess.post(f'http://{self.target}/api/user/Register', json={
                "username": username,
                "password": password,
                "secretQuestion": secret_question,
                "secretAnsver": secret_answer
            }) as response:
                assert response.status == 200, await response.read()
                return True

    async def login(self, sess: ClientSession, username: str, password: str) -> UserInfo | None:
        with try_action(self, f"Не удалось войти за {username}"):
            async with sess.post(f'http://{self.target}/api/user/Login', json={
                "password": password,
                "username": username,
            }) as response:
                assert response.status == 200, await response.read()
                json_resp = await response.json()
                return UserInfo(
                    id=json_resp["id"],
                    auth=json_resp["sessionToken"]
                )
    
    async def get_profile(self, sess: ClientSession, user: UserInfo) -> dict[str, Any]:
        with try_action(self, f"Не удалось получить профиль пользователя {user.id}"):
            async with sess.post(f'http://{self.target}/api/user/Profile', json={
                "userId": user.id,
                "sessionToken": user.auth,
            }) as response:
                assert response.status == 200, await response.read()
                return await response.json()


    async def create_ad(self, sess: ClientSession, user: UserInfo, title: str, description: str, price: int) -> bool:
        with try_action(self, "Не удалось создать объявление"):
            async with sess.post(f'http://{self.target}/api/advertisement', json={
                "title": title,
                "description": description,
                "price": price,
                "userId": user.id,
                "sessionToken": user.auth,
            }) as response:
                assert response.status == 200, await response.read()
                return True

    async def get_all_ads(self, sess: ClientSession) -> list[Any]:
        with try_action(self, "Не удалось получить список всех товаров"):
            async with sess.get(f'http://{self.target}/api/advertisement') as response:
                assert response.status == 200
                return await response.json()

    async def get_ad(self, sess: ClientSession, ad_id: int) -> dict[str, Any]:
        with try_action(self, f"Не удалось получить товар {ad_id}"):
            async with sess.get(f'http://{self.target}/api/advertisement/{ad_id}') as response:
                assert response.status == 200, await response.read()
                return await response.json()

    async def buy_ad(self, sess: ClientSession, user: UserInfo, ad_id: int) -> str:
        with try_action(self, f"Не удалось купить товар {ad_id} пользователем {user.id}"):
            async with sess.post(f'http://{self.target}/api/advertisement/Buy', json={
                "advertisementId": ad_id,
                "userId": user.id,
                "sessionToken": user.auth,
            }) as response:
                assert response.status == 200, await response.read()
                json_resp = await response.json()
                return json_resp["result"]
