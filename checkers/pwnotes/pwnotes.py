from secrets import token_hex
from typing import Any, TypedDict

import aiohttp
from aiohttp import ClientSession
from faker.providers import lorem, profile, user_agent
from faker.providers.misc import Provider as MiscProvider

from lib.checkers import BaseServiceChecker, try_action


class Credentials(TypedDict):
    username: str
    password: str
    flag: str


class Checker(BaseServiceChecker):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fake_en.add_provider(profile)
        self.fake_en.add_provider(user_agent)
        self.fake_en.add_provider(lorem)
        self.fake_en.add_provider(MiscProvider)
        self.old_credentials: Credentials
        self.credentials: Credentials

    async def check(self):
        cookie_jar = aiohttp.CookieJar(unsafe=True)
        async with aiohttp.ClientSession(
            loop=self.event_loop,
            cookie_jar=cookie_jar,
            headers={'User-Agent': self.fake_en.user_agent()},
        ) as sess:
            if not await self.fetch_main_page(sess):
                return
            
            self.logger.debug("Main page available")
            self.status += 15

            if set(["username", "password", "flag"]) <= set(self.old_credentials.keys()):
                await self.check_old_flag()
            else:
                self.messages.append("Нет данных о предыдущем флаге")

            profile = self.fake_en.profile()
            profile["username"] += token_hex(4)
            user_password = self.fake_en.password(length=6, special_chars=False)
            if not await self.register(
                sess,
                username=profile["username"],
                password=user_password,
            ):
                return
            
            self.logger.debug("Registered")
            self.credentials["username"] = profile["username"]
            self.credentials["password"] = user_password
            self.status += 15

            if not await self.login(
                sess,
                username=profile["username"],
                password=user_password,
            ):
                return
            
            self.logger.debug("Logged in")
            self.status += 10

            flag = self.generate_flag()
            if not await self.send_note(
                sess,
                topic=self.fake_en.word() + token_hex(12),
                comment=flag,
            ):
                return
            
            self.logger.debug("Note sent")
            self.status += 15

            if not (notes := await self.get_notes(sess)):
                return
            
            with try_action(self, "Добавленная заметка недоступна"):
                assert flag in [note["note"] for note in notes["notes"]]
                self.logger.debug("Flag found in notes")
                self.status += 15
                self.credentials["flag"] = flag

    async def check_old_flag(self) -> None:
        cookie_jar = aiohttp.CookieJar(unsafe=True)
        async with aiohttp.ClientSession(
            loop=self.event_loop,
            cookie_jar=cookie_jar,
            headers={'User-Agent': self.fake_en.user_agent()},
        ) as sess:
            if not await self.login(
                sess=sess,
                username=self.old_credentials["username"],
                password=self.old_credentials["password"],
            ):
                return
            
            self.status += 15

            if not (notes := await self.get_notes(sess)):
                return
            
            with try_action(self, "Добавленная заметка из предыдущего раунда недоступна"):
                assert self.old_credentials["flag"] in [note["note"] for note in notes["notes"]]
                self.logger.debug("Old flag found in notes")
                self.status += 15
                self.flag_retrieved = True

    async def fetch_main_page(self, sess: ClientSession) -> None:
        with try_action(self, "Недоступна индексная страница"):
            async with sess.request('GET', f'http://{self.target}/') as response:
                assert response.status == 200
                return aiohttp.ClientResponse

    async def register(self, sess: ClientSession, username: str, password: str) -> bool:
        with try_action(self, "Не удалось зарегистрироваться"):
            async with sess.post(f'http://{self.target}/reg', data={
                "password": password,
                "username": username,
            }) as response:
                assert response.status == 200, await response.read()
                return True

    async def login(self, sess: ClientSession, username: str, password: str) -> bool:
        with try_action(self, f"Не удалось войти за {username}"):
            async with sess.post(f'http://{self.target}/login', data={
                "password": password,
                "username": username,
            }) as response:
                self.cookie = response.cookies.get('cookie').value
                sess.headers["Cookie"] = f"cookie={response.cookies.get('cookie').value}"
                assert response.status == 200
                return True

    async def send_note(self, sess: ClientSession, topic: str, comment: str) -> bool:
        with try_action(self, "Не удалось отправить заметку"):
            headers = {"Cookie": f'cookie={self.cookie}'}
            async with sess.post(
                f'http://{self.target}/note',
                data={
                    "topic": topic,
                    "comment": comment,
                },
                headers=headers
            ) as response:
                assert response.status == 200, await response.read()
                return True

    async def get_notes(self, sess: ClientSession) -> list[Any]:
        with try_action(self, "Не удалось получить заметки"):
            async with sess.get(f'http://{self.target}/note', headers={"Cookie": f'cookie={self.cookie}'}) as response:
                assert response.status == 200
                return await response.json()
