from aiohttp import ClientSession
from secrets import token_hex
from typing import Any, TypedDict
import aiohttp
from aiohttp import ClientSession

from lib.checkers import BaseServiceChecker, try_action
from faker.providers import lorem, profile, user_agent
from faker.providers.misc import Provider as MiscProvider


class UserData(TypedDict):
    login: str
    password: str


class Credentials(TypedDict):
    user_1: UserData
    user_2: UserData
    flag: str


class Client():
    def __init__(self, client: ClientSession, checker: BaseServiceChecker):
        self.client = client
        self.checker = checker

    async def check_main(self) -> bool:
        with try_action(self.checker, "Главная страница недоступна"):
            async with self.client.request('GET', f'/app') as response:
                assert response.status == 200
                return True

    async def auth(self, login: str, password: str) -> bool:
        with try_action(self.checker, f"Не удалось войти в аккаунт {login}"):
            async with self.client.request('POST', f'/api/auth/', json={
                "login": login,
                "password": password,
            }) as response:
                jsonData = await response.json()
                return jsonData['success']

    async def user_list(self) -> list[Any]:
        with try_action(self.checker, "Не удалось получить список пользователей"):
            async with self.client.request('GET', f'/api/chat/user_list/') as response:
                return await response.json()

    async def chat_list(self) -> list[Any]:
        with try_action(self.checker, "Не удалось получить список чатов"):
            async with self.client.request('GET', f'/api/chat/chat_list/') as response:
                return await response.json()

    async def message_list(self, chat_id) -> list[Any]:
        with try_action(self.checker, f"Не удалось получить список сообщений в чате {chat_id}"):
            async with self.client.request('GET', f'/api/chat/message_list/{chat_id}') as response:
                return await response.json()

    async def chat_to_user(self, login) -> dict[str, Any]:
        with try_action(self.checker, f"Не удалось создать чат с пользователем {login}"):
            async with self.client.request('POST', f'/api/chat/chat_to_user/', json={'toUser':login}) as response:
                return await response.json()

    async def message(self, chat, text) -> bool:
        with try_action(self.checker, f"Не удалось отправить сообщение в чат {chat}"):
            async with self.client.request('POST', f'/api/chat/message/', json={'chat':chat, 'text':text}) as response:
                resp_json = await response.json()
                return resp_json['success']


class Checker(BaseServiceChecker):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fake_en.add_provider(profile)
        self.fake_en.add_provider(user_agent)
        self.fake_en.add_provider(lorem)
        self.fake_en.add_provider(MiscProvider)
        self.credentials: Credentials
        self.old_credentials: Credentials
    
    async def check(self):
        async with (
            aiohttp.ClientSession(
                base_url=f"http://{self.target}",
                cookie_jar=aiohttp.CookieJar(unsafe=True),
                headers={'User-Agent': self.fake_en.user_agent()},
            ) as sess1,
            ClientSession(
                base_url=f"http://{self.target}",
                cookie_jar=aiohttp.CookieJar(unsafe=True),
                headers={'User-Agent': self.fake_en.user_agent()},
            ) as sess2,
        ):
            client = Client(sess1, self)
            client_2 = Client(sess2, self)

            if not await client.check_main():
                return
            
            self.status += 5

            if set(["user_1", "user_2", "flag"]) <= set(self.old_credentials.keys()):
                await self.check_old_flag()
            else:
                self.messages.append("Нет данных предыдущего раунда, не удалось проверить старый флаг")

            self.credentials['user_1'] = UserData(
                login=self.fake_en.profile()['username'] + token_hex(4),
                password=self.fake_en.password(length=10, special_chars=True),
            )
            self.credentials['user_2'] = UserData(
                login=self.fake_en.profile()['username'] + token_hex(4),
                password=self.fake_en.password(length=10, special_chars=True),
            )

            if not await client.auth(self.credentials['user_1']['login'], self.credentials['user_1']['password']):
                return
            
            if not await client_2.auth(self.credentials['user_2']['login'], self.credentials['user_2']['password']):
                return

            self.status += 5

            if not (user_list := await client.user_list()):
                return

            ok = False
            with try_action(self, "Не удалось обработать список пользователей"):
                ok = self.credentials['user_2']['login'] in [x['login'] for x in user_list]
                self.status += 5
            
            if not ok:
                return

            if not (chat_data := await client.chat_to_user(self.credentials['user_2']['login'])):
                return
            
            self.status += 5

            if not (chat_list_2 := await client_2.chat_list()):
                return
            
            self.status += 5

            ok = False
            with try_action(self, "Не удалось обработать список чатов"):
                ok = chat_data['chat'] in [x['id'] for x in chat_list_2]

            if not ok:
                return

            self.status += 5

            flag = self.generate_flag()

            if not await client_2.message(chat_data['chat'], flag):
                return
            
            self.status += 5

            if not (message_list := await client.message_list(chat_data['chat'])):
                return
           
            if not (message_list_2 := await client_2.message_list(chat_data['chat'])):
                return

            self.status += 5

            with try_action(self, "Не удалось найти флаг в созданном чате", add_score=30):
                assert flag in [x['text'] for x in message_list]
                assert flag in [x['text'] for x in message_list_2]
                self.credentials['flag'] = flag
    
    async def check_old_flag(self) -> None:
        userdata = self.old_credentials['user_1']
        userdata_2 = self.old_credentials['user_2']

        async with (
            aiohttp.ClientSession(
                base_url=f"http://{self.target}",
                cookie_jar=aiohttp.CookieJar(unsafe=True),
                headers={'User-Agent': self.fake_en.user_agent()},
            ) as sess1,
            ClientSession(
                base_url=f"http://{self.target}",
                cookie_jar=aiohttp.CookieJar(unsafe=True),
                headers={'User-Agent': self.fake_en.user_agent()},
            ) as sess2,
        ):
            client = Client(sess1, self)
            client_2 = Client(sess2, self)

            if not await client.auth(userdata['login'], userdata['password']):
                return
            
            if not await client_2.auth(userdata_2['login'], userdata_2['password']):
                return

            self.status += 5

            if not (chat_data := await client.chat_to_user(userdata_2['login'])):
                return
            
            self.status += 5

            if not (chat_list_2 := await client_2.chat_list()):
                return

            self.status += 5

            ok = False
            with try_action(self, "Не удалось обработать список чатов"):
                assert chat_data['chat'] in [x['id'] for x in chat_list_2]
                ok = True
            
            if not ok:
                return

            if not (message_list := await client.message_list(chat_data['chat'])):
                return
            
            if not (message_list_2 := await client_2.message_list(chat_data['chat'])):
                return
            
            self.status += 5

            with try_action(self, "Не удалось найти флаг предыдущего раунда", add_score=10):
                assert self.old_credentials['flag'] in [x['text'] for x in message_list]
                assert self.old_credentials['flag'] in [x['text'] for x in message_list_2]
                self.flag_retrieved = True
