#!/bin/bash

piccolo migrations forwards app
uvicorn server:app --port 8000 --host 0.0.0.0 --log-level debug
