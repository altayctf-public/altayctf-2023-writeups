from piccolo.conf.apps import AppRegistry
from piccolo.engine.postgres import PostgresEngine


DB = PostgresEngine(config={
        "database": "p2pchat",
        "user": "user",
        "password": "password",
        "host": "p2pchat_db",
        "port": 5432,
})


# A list of paths to piccolo apps
# e.g. ['blog.piccolo_app']
APP_REGISTRY = AppRegistry(apps=['app.piccolo_app'])
