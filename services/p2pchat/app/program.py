from blacksheep import Route
from blacksheep.server import Application
from config.common import Configuration
from rodi import Container

from . import controllers  # NoQA
from .auth import configure_authentication
from .errors import configure_error_handlers
from .piccolo import configure_piccolo


async def before_start(application: Application) -> None:
    application.services.add_instance(application)
    application.services.add_alias("app", Application)

def configure_application(
    services: Container,
    configuration: Configuration,
) -> Application:
    app = Application(
        services=services,
        show_error_details=configuration.show_error_details,
        debug=configuration.debug,
    )

    app.on_start += before_start

    configure_error_handlers(app)
    configure_authentication(app)
    configure_piccolo(app)

    app.serve_files("app/static", root_path='app')

    Route.value_patterns["no_api"] = r"(?!api/).*"

    return app
