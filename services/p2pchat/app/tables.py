from piccolo.columns import Varchar, Boolean, Text, ForeignKey, M2M, LazyTableReference
from piccolo.table import Table

class Companion(Table):
    login = Varchar(index=True)
    password = Varchar()
    chats = M2M(LazyTableReference("CompanionToChat", module_path=__name__))

class Chat(Table):
    companions = M2M(LazyTableReference("CompanionToChat", module_path=__name__))

class CompanionToChat(Table):
    companion = ForeignKey(Companion)
    chat = ForeignKey(Chat)

class Message(Table):
    chat = ForeignKey(Chat)
    by_companion = ForeignKey(Companion)
    text = Text()
