from typing import Tuple

from config.common import Configuration
from rodi import Container
from app.ws import ConnectionManager


def configure_services(
    configuration: Configuration,
) -> Tuple[Container, Configuration]:
    container = Container()

    container.add_instance(configuration)
    container.add_singleton(ConnectionManager)

    return container, configuration
