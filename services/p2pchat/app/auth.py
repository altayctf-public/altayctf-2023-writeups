from typing import Optional

from hashlib import sha512
from blacksheep import Request
from blacksheep.server.authorization import Policy
from guardpost import Identity
from guardpost.asynchronous.authentication import AuthenticationHandler
from guardpost.common import AuthenticatedRequirement
from app.tables import Companion


def makeAuthString(login):
    return login.strip() + ':' + sha512(login.strip().encode() + 'secret_string'.encode()).hexdigest()


class AppAuthenticationHandler(AuthenticationHandler):
    async def authenticate(self, context: Request) -> Optional[Identity]:
        auth_string = context.get_cookie('auth_string')

        if not auth_string:
            context.identity = None

            return context.identity

        login = auth_string.split(':')[0]
        user = None

        if auth_string == makeAuthString(login):
            user = await Companion.select().where(Companion.login == login).first()

        if user:
            context.identity = Identity(user, authentication_mode='auth')
        else:
            context.identity = None

        return context.identity


def configure_authentication(app):
    app.use_authentication().add(AppAuthenticationHandler())

    app.use_authorization().add(Policy('auth', AuthenticatedRequirement()))

