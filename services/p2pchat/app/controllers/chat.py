from blacksheep import Request, WebSocket
from blacksheep.server.controllers import APIController, get, post, ws
from blacksheep.server.authorization import auth
from app.tables import Companion, Chat as ChatTable, Message, CompanionToChat
from app.ws import ConnectionManager

class Chat(APIController):
    @auth('auth')
    @get('user_list')
    async def user_list(self, request: Request):
        return await Companion.select(Companion.login)\
            .where(Companion.id != request.identity['id'])

    @auth('auth')
    @get('chat_list')
    async def chat_list(self, request: Request):
        ids = await CompanionToChat\
            .select(CompanionToChat.chat)\
            .where(CompanionToChat.companion == request.identity['id'])

        if not ids:
            return []

        chats = await ChatTable\
            .select(ChatTable.id, ChatTable.companions(Companion.login))\
            .where(ChatTable.id.is_in([x['chat'] for x in ids]))

        return chats

    @auth('auth')
    @get('message_list/{chat_id}')
    async def message_list(self, chat_id: int):
        if not chat_id:
            return {
                'success': False,
                'error': 422,
                'message': 'Неверный id чата',
            }

        return await Message.select(Message.all_columns(), Message.by_companion.login)\
            .where(Message.chat == chat_id).output(nested=True)

    @auth('auth')
    @post('chat_to_user')
    async def chat_to_user(self, request: Request):
        data = await request.json()

        to_user_login = str(data['toUser'])

        to_user = await Companion.select()\
            .where(Companion.login == to_user_login)\
            .first()

        if not to_user:
            return {
                'success': False,
                'error': 422,
                'message': 'Пользователь не найден',
            }

        chatIdList = await CompanionToChat\
            .select(CompanionToChat.chat.id)\
            .where(CompanionToChat.companion == request.identity['id'])

        chatIdList = [x['chat.id'] for x in chatIdList]

        chatId = None

        if chatIdList:
            chatId = await CompanionToChat \
                .select(CompanionToChat.chat.id) \
                .where(CompanionToChat.chat.id.is_in(chatIdList))\
                .where(CompanionToChat.companion == to_user['id'])\
                .first()

        if (chatId):
            chatId = chatId['chat.id']
        else:
            result = await ChatTable.insert(ChatTable()).returning(*ChatTable.all_columns())

            chatId = result[0]['id']

            await CompanionToChat.insert(CompanionToChat(companion=request.identity['id'], chat=chatId))
            await CompanionToChat.insert(CompanionToChat(companion=to_user['id'], chat=chatId))

        return {
            'success': True,
            'chat': chatId,
            'messages': await Message.select(Message.all_columns(), Message.by_companion.login).where(Message.chat == chatId).output(nested=True)
        }

    @auth('auth')
    @post('message')
    async def message(self, request: Request, manager: ConnectionManager):
        data = await request.json()

        chat_id = data['chat']

        chat = await ChatTable\
            .select(ChatTable.all_columns(), ChatTable.companions(Companion.id))\
            .where(ChatTable.id == chat_id)\
            .first()

        if not chat or (request.identity['id'] not in [x['id'] for x in chat['companions']]):
            return {
                'success': False,
                'error': 422,
                'message': 'Неверный id чата',
            }

        messages = await Message\
            .insert(Message(chat=chat['id'], by_companion=request.identity['id'], text=data['text']))\
            .returning(Message.id)

        messages = await Message.select(Message.all_columns(), Message.by_companion.login).where(Message.id == messages[0]['id']).output(
            nested=True).first()

        await manager.message(messages)

        return {
            'success': True,
        }

    @auth('auth')
    @ws('ws/{chat_id}')
    async def ws(websocket: WebSocket, chat_id: int, manager: ConnectionManager):
        chat = await ChatTable\
            .select(ChatTable.all_columns(), ChatTable.companions(Companion.id))\
            .where(ChatTable.id == chat_id)\
            .first()

        if not chat or (websocket.identity['id'] not in [x['id'] for x in chat['companions']]):
            return await websocket.close()

        conn = await manager.connect(websocket, websocket.identity['login'], chat['id'])

        try:
            while True:
                await manager.manage(conn)
        except:
            await manager.disconnect(conn)
