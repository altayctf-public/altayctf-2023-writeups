from blacksheep.server.controllers import Controller, get
from blacksheep.server.responses import redirect


class Home(Controller):
    @get('/{no_api:route}')
    def index(self):
        return redirect('/app')
