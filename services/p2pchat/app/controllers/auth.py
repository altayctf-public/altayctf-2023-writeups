from uuid import UUID

from blacksheep import json, Request
from blacksheep.cookies import Cookie
from blacksheep.server.controllers import APIController, post

from app.auth import makeAuthString
from app.tables import Companion

class Auth(APIController):
    @post()
    async def auth(self, request: Request):
        data = await request.json()

        if not data or not data['login'] or not data['password']:
            return {
                'success': False,
                'error': 422,
                'message': 'Пустой логин или пароль',
            }

        user = await Companion.select().where((Companion.login == data['login'])).first()

        if not user:
            result = await Companion.insert(Companion(login=data['login'], password=data['password']))

            userId = result[0]['id']

            user = await Companion.select().where((Companion.id == userId)).first()

        if user['password'] != data['password']:
            return {
                'success': False,
                'error': 422,
                'message': 'Неправильный логин или пароль',
            }

        response = json({
            'success': True,
        })

        response.set_cookie(Cookie(
            'auth_string',
            makeAuthString(user['login']),
            path='/',
        ))

        return response

