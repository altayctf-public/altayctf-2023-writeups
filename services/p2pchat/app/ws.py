from typing import List

from blacksheep import WebSocket


class Connection:
    def __init__(self, socket: WebSocket, login: str, chat: str):
        self.socket = socket
        self.login = login
        self.chat = chat

    async def receive(self):
        data = await self.socket.receive_json()
        return data

    async def send(self, message):
        await self.socket.send_json(message)


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[Connection] = []

    async def connect(self, websocket: WebSocket, login: str, chat: str):
        await websocket.accept()
        connection = Connection(websocket, login, chat)
        self.active_connections.append(connection)
        return connection

    async def disconnect(self, connection: Connection):
        self.active_connections.remove(connection)

    async def manage(self, connection: Connection):
        await connection.receive()

    async def message(self, message):
        for connection in self.active_connections:
            if connection.chat == message['chat']:
                await connection.send(message)