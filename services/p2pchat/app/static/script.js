WS_PROTO = 'ws'
WS_URL = `${WS_PROTO}://${location.host}/api/chat/ws/`

async function api(url, params) {
    const method = params?.method !== undefined ? params?.method.toUpperCase() : 'GET'

    const res = await fetch('/api/' + url, {
        method: method,
        credentials: 'include',
        mode: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
        },
        body:
            method === 'POST'
                ? JSON.stringify(params?.params)
                : undefined,
    })

    if (!res.ok) {
        alert('Произошла ошибка')
        throw {...res}
    }

    const resJson = await res.json()

    if (resJson.success === false) {
        alert(resJson.message ?? 'Произошла ошибка')
        throw {...res}
    }

    return resJson
}

const app = {
    data() {
        return {
            users: [],
            selectedUser: null,
            chats: [],
            selectedChat: null,
            messages: [],

            messageText: "",

            ws: null,
            login: decodeURIComponent(
                document.cookie.split("; ")
                    .find((row) => row.startsWith("auth_string="))
                    ?.split("=")[1] ?? ''
            )?.split(':')[0] ?? null,
            inputLogin: '',
            inputPassword: '',
        }
    },
    methods: {
        async auth() {
            const res = await api('auth', {
                method: 'post',
                params: {
                    login: this.inputLogin,
                    password: this.inputPassword,
                }
            });

            this.login = decodeURIComponent(
                document.cookie.split("; ")
                    .find((row) => row.startsWith("auth_string="))
                    ?.split("=")[1] ?? ''
            )?.split(':')[0] ?? null;

            this.getUserList();
            this.getChatsList();
        },

        async logout() {
            this.disconnect();

            document.cookie = 'auth_string=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';

            this.login = null;
            this.inputLogin = '';
            this.inputPassword = '';

            this.users = [];
            this.selectedUser = null;
            this.chats = [];
            this.selectedChat = null;
            this.messages = [];

            this.messageText = "";
        },

        async getUserList() {
            const res = await api('chat/user_list');

            this.users = res;
        },

        async getChatsList() {
            const res = await api('chat/chat_list');

            this.chats = res;
        },

        async selectChat(chat) {
            const res = await api(`chat/message_list/${chat}`);

            this.selectedUser = null;
            this.messages = res;
            this.selectedChat = chat;
        },

        async chatToUser(login) {
            const res = await api('chat/chat_to_user', {
                method: 'post',
                params: {
                    toUser: login,
                }
            });

            this.selectedUser = login;
            this.selectedChat = res.chat;
            this.messages = res.messages;
        },

        async sendMessage() {
            if (!this.selectedChat || !this.messageText) return;

            await api('chat/message', {
                method: 'post',
                params: {
                    chat: this.selectedChat,
                    text: this.messageText,
                }
            });

            this.messageText = '';
        },

        connect(url) {
            if (this.ws) {
                this.ws.close()
            }

            const ws = new WebSocket(url)

            ws.addEventListener("open", (evt) => {
                console.log("Open", evt)
            })

            ws.addEventListener("message", (evt) => {
                this.messages.push(JSON.parse(evt.data))
                console.log("Message", evt)
            })

            ws.addEventListener("close", (evt) => {
                console.log("Close", evt)
            })

            ws.addEventListener("error", (evt) => {
                console.error("Error", evt)
            })

            this.ws = ws
        },

        disconnect() {
            if (this.ws) {
                this.ws.close()
            }
            this.ws = null
        },
    },

    watch: {
        selectedChat() {
            if (!this.selectedChat) {
                this.disconnect();

                return;
            }

            this.connect(WS_URL + this.selectedChat)
        }
    },

    async mounted() {
        if (this.login){
            this.getUserList();
            this.getChatsList();
        }
    },

    beforeUnmount() {
        this.disconnect()
    }
}

Vue.createApp(app).mount("#app")