# P2PChat

## Description
Обычный чатик на велосипедах. P@P


## Idor
В метод `app/controllers/chat.py:32` `message_list` передается id чата. 
Но метод не содержит проверок доступа к чату.

Можно перебирать все id чатов и получать доступ к всем сообщениям.
(Создав новый чат мы можем узнать последний актуальный id)


## Fast fix
Необходимо добавить проверку доступа. 

Можно из метода `chat_list` (получает все id чатов, к которым имеет доступ текущий пользователь) 
взять выборку чатов. Добавить к ней фильтр по id чата.
После проверить что результат не пустой.


```python
@auth('auth')
@get('message_list/{chat_id}')
async def message_list(self, request: Request, chat_id: int):
    if not chat_id:
        return {
            'success': False,
            'error': 422,
            'message': 'Неверный id чата',
        }

    chat_check = await CompanionToChat\
        .select(CompanionToChat.chat)\
        .where(CompanionToChat.chat.id == chat_id)\
        .where(CompanionToChat.companion == request.identity['id'])\
        .first()

    if not chat_check:
        return {
            'success': False,
            'error': 422,
            'message': 'Нет доступа к чату',
        }

    return await Message.select(Message.all_columns(), Message.by_companion.login)\
        .where(Message.chat == chat_id).output(nested=True)
```


## Missconfig

Куки авторизации формируются в `/app/auth.py:13`. 
Там используется строка `secret_string`. Раз она зашита в коде, она одинаковая у всех команд.

Можно вычислить куки авторизации под любого пользователя.

(Список логинов может получить любой зарегистрированный пользователь по запросу `/api/chat/user_list`)

## Fast fix
Для исправления нужно изменить строку `secret_string` на уникальную строку.


## Strip
Куки авторизации формируются в `/app/auth.py:13`. 
Можно увидеть `login.strip()` в генерации куки авторизации.
Но при регистрации/логине `app/controllers/auth.py:25` никаких `strip()` с логином не производится.

Если зарегистрироваться/залогиниться с логином как у другого пользователя, но добавить пробелы в начало или конец логина, то нас авторизует под этим другим пользователем.

(Список логинов может получить любой зарегистрированный пользователь по запросу `/api/chat/user_list`)

## Fast fix
убрать функцию `strip`

