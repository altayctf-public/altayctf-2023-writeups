<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property-read int $id
 * @property string $name
 * @property string $lang_direction
 * @property string $description
 * @property bool $is_private
 * @property Translation[] $translations
 */
class Dictionary extends Model
{
    use HasFactory;

    protected $table = 'dictionaries';

    protected $fillable = [
        'user_id',
        'name',
        'lang_direction',
        'description',
        'is_private',
    ];

    public function translations(): HasMany
    {
        return $this->hasMany(Translation::class);
    }
}
