<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Translation extends Model
{
    use HasFactory;

    protected $table = 'translations';

    protected $fillable = [
        'dictionary_id',
        'original_word',
        'translation',
        'note',
    ];

    public $timestamps = false;

    public function dictionary(): BelongsTo
    {
        return $this->belongsTo(Dictionary::class);
    }

    /**
     * @return Collection
     */
    public static function getLast(int $limit = 20): Collection
    {
        return self::whereRelation('dictionary', 'is_private', false)->limit($limit)->get();
    }
}
