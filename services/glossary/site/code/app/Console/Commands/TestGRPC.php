<?php

namespace App\Console\Commands;

use App\Services\Translator;
use App\Services\TranslatorClient\LangPair;
use App\Services\TranslatorClient\Query;
use App\Services\TranslatorClient\TranslateServiceClient;
use App\Services\TranslatorClient\TranslationResult;
use Grpc\ChannelCredentials;
use Illuminate\Console\Command;

class TestGRPC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:test-grpc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var Translator $translator */
        $translator = resolve(Translator::class);
        $result = $translator->handle('windows timer', LangPair::LANGPAIR_EN_RU);
        dd($result);
    }
}
