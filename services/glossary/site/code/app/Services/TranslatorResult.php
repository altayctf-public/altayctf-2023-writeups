<?php

namespace App\Services;

class TranslatorResult
{
    /**
     * @var string
     */
    protected string $originalText;

    /**
     * @var string
     */
    protected string $translatedText;

    /**
     * @var array
     */
    protected array $translationVariants = [];

    /**
     * @return string
     */
    public function getOriginalText(): string
    {
        return $this->originalText;
    }

    /**
     * @param string $originalText
     */
    public function setOriginalText(string $originalText): void
    {
        $this->originalText = $originalText;
    }

    /**
     * @return string
     */
    public function getTranslatedText(): string
    {
        return $this->translatedText;
    }

    /**
     * @param string $translatedText
     */
    public function setTranslatedText(string $translatedText): void
    {
        $this->translatedText = $translatedText;
    }

    /**
     * @return array
     */
    public function getTranslationVariants(): array
    {
        return $this->translationVariants;
    }

    /**
     * @param array $translationVariants
     */
    public function setTranslationVariants(array $translationVariants): void
    {
        $this->translationVariants = $translationVariants;
    }
}
