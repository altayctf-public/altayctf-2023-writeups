<?php

namespace App\Services;

use App\Exceptions\TranslatorException;
use App\Services\TranslatorClient\Query;
use App\Services\TranslatorClient\TranslateServiceClient;
use App\Services\TranslatorClient\TranslationResult;

class Translator
{
    const STATUS_SUCCESS = 0;

    /**
     * @var TranslateServiceClient
     */
    protected TranslateServiceClient $client;

    public function __construct(TranslateServiceClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $text
     * @param string $langDirection
     * @return TranslatorResult
     * @throws TranslatorException
     */
    public function handle(string $text, string $langDirection): TranslatorResult
    {
        $request = new Query();
        $request->setText($text);
        $request->setLangPair($langDirection);

        list($translationResult, $status) = $this->client->TranslateText($request)->wait();
        if ($status->code !== self::STATUS_SUCCESS) {
            throw new TranslatorException($status->details);
        }

        return $this->mapValues($translationResult);
    }

    /**
     * @param TranslationResult $translationResult
     * @return TranslatorResult
     */
    protected function mapValues(TranslationResult $translationResult): TranslatorResult
    {
        $result = new TranslatorResult();
        $result->setOriginalText($translationResult->getSourceText());
        $result->setTranslatedText($translationResult->getTranslatedText());

        $variants = [];
        $translatedWords = $translationResult->getTranslatedWords();
        foreach ($translatedWords as $translationVariants) {
            $synonyms = $translationVariants->getSynonyms();
            foreach ($synonyms as $synonym) {
                $variants[$translationVariants->getSourceWord()][] = $synonym;
            }
        }

        $result->setTranslationVariants($variants);

        return $result;
    }
}
