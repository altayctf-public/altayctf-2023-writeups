<?php
// GENERATED CODE -- DO NOT EDIT!

namespace App\Services\TranslatorClient;

/**
 */
class TranslateServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \App\Services\TranslatorClient\Query $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function TranslateText(\App\Services\TranslatorClient\Query $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/TranslateService/TranslateText',
        $argument,
        ['\App\Services\TranslatorClient\TranslationResult', 'decode'],
        $metadata, $options);
    }

}
