<?php

namespace App\Providers;

use App\Services\TranslatorClient\TranslateServiceClient;
use Grpc\ChannelCredentials;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class TranslatorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(TranslateServiceClient::class, function (Application $app) {
            return new TranslateServiceClient(config('translator.url') . ':' . config('translator.port'), ['credentials' => ChannelCredentials::createInsecure()]);
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {

    }
}
