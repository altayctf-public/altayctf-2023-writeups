<?php

namespace App\Providers;

use App\Models\Dictionary;
use App\Models\Translation;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('edit-dictionary', function (User $user, Dictionary $dictionary) {
            return (($user->id !== null) || ($user->id === $dictionary->user_id));
        });
        Gate::define('update-translation', function (User $user, Dictionary $dictionary, Translation $translation) {
            return ($user->id === $dictionary->user_id) && ($dictionary->id === $translation->dictionary_id);
        });
    }
}
