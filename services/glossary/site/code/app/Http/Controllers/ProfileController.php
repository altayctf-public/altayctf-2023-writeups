<?php

namespace App\Http\Controllers;

use App\Models\Dictionary;
use App\Models\Translation;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('profile.main', ['user' => Auth::user()]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateProfile(Request $request): RedirectResponse
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . Auth::user()->id . '|email:rfc',
        ];

        if ($request->get('password') !== null) {
            $rules = Arr::add($rules, 'password', 'required|min:8');
        }

        $validated = $request->validate($rules);

        $profileInfo = [
            'name' => $validated['name'],
            'email' => $validated['email'],
        ];

        if (isset($validated['password'])) {
            $profileInfo = Arr::add($profileInfo, 'password', Hash::make($validated['password'], [
                'rounds' => 12
            ]));
        }

        Auth::user()->update($profileInfo);

        return back()->with('message', 'Profile info updated successfully');
    }

    public function dictionaries()
    {
        return view('profile.dictionaries', ['dictionaries' => Auth::user()->dictionaries]);
    }

    public function dictionaryCreationForm()
    {
        return view('profile.create_dictionary_form');
    }

    public function dictionaryEditForm($id)
    {
        /** @var Dictionary $dict */
        $dict = Dictionary::find($id);

        if ($dict === null) {
            return back();
        }

        if (Gate::allows('edit-dictionary', $dict)) {
            return view('profile.edit_dictionary_form', ['dictionary' => $dict]);
        } else {
            abort(403);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function createDictionary(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'name' => 'required',
            'lang_direction' => 'required',
            'is_private' => 'required|boolean',
        ]);

        /** @var User $user */
        $user = Auth::user();

        /** @var Dictionary $dict */
        $dict = $user->dictionaries()->create([
            'name' => $validated['name'],
            'lang_direction' => $validated['lang_direction'],
            'description' => $request->get('description'),
            'is_private' => (bool) $validated['is_private'],
        ]);

        return redirect()->route('user.profile.edit_dictionary_form', ['id' => $dict->id]);
    }

    public function updateDictionary($id, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'lang_direction' => 'required',
            'is_private' => 'required|boolean',
        ]);

        /** @var User $user */
        $user = Auth::user();

        /** @var Dictionary $dict */
        $dict = $user->dictionaries()
            ->find($id);

        if (null === $dict) {
            return back();
        }

        $dict->update([
            'name' => $validated['name'],
            'lang_direction' => $validated['lang_direction'],
            'description' => $request->get('description'),
            'is_private' => (bool) $validated['is_private'],
        ]);

        return back()->with('message', "Dictionary updated successfully");
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function deleteDictionary(int $id): RedirectResponse
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Dictionary $dict */
        $dict = $user->dictionaries()->find($id);
        $dict->translations()->delete();
        $dict->delete();

        return back();
    }

    /**
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function addTranslation(int $id, Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'original_word' => 'required',
            'translation' => 'required',
        ]);

        /** @var User $user */
        $user = Auth::user();

        /** @var Dictionary $dict */
        $dict = $user->dictionaries()->find($id);
        if (null === $dict) {
            return back();
        }

        $dict->translations()->create([
            'original_word' => $validated['original_word'],
            'translation' => $validated['translation'],
        ]);

        return back();
    }

    public function updateTranslation(int $id, Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'translation_id' => 'required',
            'original_word' => 'required',
            'translation' => 'required',
        ]);

        $translationId = $validated['translation_id'];

        /** @var User $user */
        $user = Auth::user();

        /** @var Dictionary $dict */
        $dict = $user->dictionaries()->find($id);
        if (null === $dict) {
            return back();
        }

        /** @var Translation $translation */
        $translation = Translation::find($translationId);

        if (Gate::allows('update-translation', [$dict, $translation])) {
            $translation->update([
                'original_word' => $validated['original_word'],
                'translation' => $validated['translation'],
            ]);
        } else {
            abort(403);
        }

        return back();
    }
}
