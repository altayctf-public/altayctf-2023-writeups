<?php

namespace App\Http\Controllers;

use App\Models\Translation;
use App\Services\Translator;
use App\Services\TranslatorResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SiteController
{
    public function homepage()
    {
        return view('site.homepage', [
            'lastTranslations' => Translation::getLast()
        ]);
    }

    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => 'required',
            'lang_direction' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('homepage');
        }

        $validated = $validator->validated();

        $translationResult = new TranslatorResult();

        try {
            /** @var Translator $translator */
            $translator = resolve(Translator::class);
            $translationResult = $translator->handle($validated['text'], $validated['lang_direction']);
            $statusCode = 200;
        } catch (\Throwable $t) {
            Log::error('[SiteController::search] ' . $t->getMessage());
            session()->flash('error', 'Translation service temporarily unavailable');
            $statusCode = 503;
        } finally {
            return response()->view('site.search_results', ['translationResult' => $translationResult])->setStatusCode($statusCode);
        }
    }
}
