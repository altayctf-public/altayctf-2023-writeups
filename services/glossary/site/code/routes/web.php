<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [Controllers\SiteController::class, 'homepage'])->name('homepage');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', [Controllers\UserController::class, 'loginForm'])->name('user.login_form');
    Route::get('/register', [Controllers\UserController::class, 'registrationForm'])->name('user.registration_form');
    Route::post('/login', [Controllers\UserController::class, 'login'])->name('user.do_login');
    Route::post('/register', [Controllers\UserController::class, 'register'])->name('user.do_register');
});

Route::group(['middleware' => 'auth'], function () {
    Route::post('/logout', [Controllers\UserController::class, 'logout'])->name('user.logout');
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [Controllers\ProfileController::class, 'index'])->name('user.profile');
        Route::post('/update', [Controllers\ProfileController::class, 'updateProfile'])->name('user.profile.update');

        Route::group(['prefix' => 'dictionaries'], function () {
            Route::get('/', [Controllers\ProfileController::class, 'dictionaries'])->name('user.profile.dictionaries');
            Route::get('/create', [Controllers\ProfileController::class, 'dictionaryCreationForm'])->name('user.profile.create_dictionary_form');
            Route::get('/{id}', [Controllers\ProfileController::class, 'dictionaryEditForm'])->name('user.profile.edit_dictionary_form');
            Route::post('/create', [Controllers\ProfileController::class, 'createDictionary'])->name('user.profile.create_dictionary');
            Route::post('/{id}', [Controllers\ProfileController::class, 'updateDictionary'])->name('user.profile.update_dictionary');
            Route::delete('/{id}', [Controllers\ProfileController::class, 'deleteDictionary'])->name('user.profile.delete_dictionary');
            Route::post('/{id}/translation', [Controllers\ProfileController::class, 'addTranslation'])->name('user.profile.add_translation');
            Route::post('/{id}/translation/update', [Controllers\ProfileController::class, 'updateTranslation'])->name('user.profile.update_translation');
        });
    });
});

Route::get('/search', [Controllers\SiteController::class, 'search'])->name('site.search');
