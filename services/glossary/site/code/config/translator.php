<?php

return [
    'url' => env('TRANSLATOR_URL'),
    'port' => env('TRANSLATOR_PORT'),
];
