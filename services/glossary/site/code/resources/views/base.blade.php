<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Glossary - translation service</title>
    <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    @yield('css')
</head>
<body>
    @include('navbar')
    <div class="container">
        @yield('content')
    </div>
    <script src="/js/bootstrap/bootstrap.bundle.min.js"></script>
    @yield('js')
</body>
</html>
