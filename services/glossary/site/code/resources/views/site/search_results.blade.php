@extends('base')

@section('content')
    <div class="row mt-5">
        <div class="col">
            <h4 class="text-center">Search results</h4>
        </div>
    </div>
    @if (session()->has('error'))
        <div class="row mt-5">
            <div class="col col-md-6 offset-md-3">
                <div class="alert alert-danger text-center">
                    {{ session()->get('error') }}
                </div>
            </div>
        </div>
    @else
        <div class="row mt-5">
            <div class="col col-md-6 offset-md-3">
                <h5 class="text-center">Translated text</h5>
                <table class="table table-striped-columns mt-3">
                    <thead>
                        <tr>
                            <th scope="col">Original text</th>
                            <th scope="col">Translated text</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $translationResult->getOriginalText() }}</td>
                            <td>{{ $translationResult->getTranslatedText() }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @if (!empty($translationResult->getTranslationVariants()))
        <div class="row mt-5">
            <div class="col col-md-6 offset-md-3">
                <h5 class="text-center">Translation variants</h5>
                <table class="table table-striped-columns mt-3">
                    <thead>
                    <tr>
                        <th scope="col">Word</th>
                        <th scope="col">Synonyms</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($translationResult->getTranslationVariants() as $sourceWord => $translations)
                        <tr>
                            <td>{{ $sourceWord }}</td>
                            <td>{{ implode(', ', $translations) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    @endif
@endsection
