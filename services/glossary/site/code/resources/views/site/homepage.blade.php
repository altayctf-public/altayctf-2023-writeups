@extends('base')

@section('content')
    <div class="row mt-5">
        <div class="col">
            <h4 class="text-center">Welcome to Glossary - the best translation service in the world</h4>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-6 offset-md-3">
            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                </div>
                <div class="carousel-inner"  style=" width:100%; height: 400px !important;">
                    <div class="carousel-item active">
                        <img src="{{ asset('/images/gb.png') }}" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block text-bg-warning">
                            <h5>From English to Russian</h5>
                            <p>You can translate any text from English to Russian and vice versa</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('/images/security.png') }}" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block text-bg-warning">
                            <h5>Data protection above all</h5>
                            <p>Your private dictionaries are under reliable protection</p>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col col-md-6 offset-md-3">
            <h5 class="text-center">Last users' translations</h5>
            <table class="table border-bottom border-warning">
                <thead>
                    <tr>
                        <th scope="col">Original word or phrase</th>
                        <th scope="col">Translation</th>
                        <th scope="col">Note</th>
                    </tr>
                </thead>
                @foreach($lastTranslations as $translation)
                    <tr>
                        <td>{{ $translation->original_word }}</td>
                        <td>{{ $translation->translation }}</td>
                        <td>{{ $translation->note }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
