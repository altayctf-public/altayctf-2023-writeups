@extends('base')

@section('content')
    <div class="row mt-5">
        <div class="col">
            <h3 class="text-center">Profile</h3>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col col-md-3">
            @include('profile/sidebar')
        </div>
        <div class="col offset-md-1 col-md-7">
            <h4>Edit dictionary</h4>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('message'))
                <div class="alert alert-info">
                    {{ session()->get('message') }}
                </div>
            @endif
            <form action="{{ route('user.profile.update_dictionary', ['id' => $dictionary->id]) }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputName" class="form-label">Name</label>
                    <input type="text" class="form-control" id="exampleInputName" name="name" value="{{ $dictionary->name }}" required>
                </div>
                <div class="mb-3">
                    <label for="langDirection" class="form-label">Languages pair</label>
                    <select class="form-select" id="langDirection" aria-label="Languages pair" name="lang_direction">
                        <option value="en-ru" @if($dictionary->lang_direction === 'en-ru') selected @endif>EN-RU</option>
                        <option value="ru-en" @if($dictionary->lang_direction === 'ru-en') selected @endif>RU-EN</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlDesc" class="form-label">Description</label>
                    <textarea class="form-control" id="exampleFormControlDesc" rows="3" name="description">{{ $dictionary->description }}</textarea>
                </div>
                <div class="mb-3">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="is_private" id="inlinePublic" value="0" @if (!$dictionary->is_private) checked @endif>
                        <label class="form-check-label" for="inlinePublic">Public</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="is_private" id="inlinePrivate" value="1" @if ($dictionary->is_private) checked @endif>
                        <label class="form-check-label" for="inlinePrivate">Private</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-warning">Update</button>
            </form>
            <h5 class="mt-5">Translations</h5>
            @foreach($dictionary->translations as $translation)
                <form action="{{ route('user.profile.update_translation', ['id' => $dictionary->id]) }}" method="post" class="mt-3">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="hidden" name="translation_id" value="{{ $translation->id }}">
                        <input type="text" class="form-control" name="original_word" placeholder="Original word" aria-label="Original word" value="{{ $translation->original_word }}">
                        <input type="text" class="form-control" name="translation" placeholder="Translation" aria-label="Translation" value="{{ $translation->translation }}">
                        <button type="submit" class="btn btn-warning">Update</button>
                    </div>
                </form>
            @endforeach
            <form action="{{ route('user.profile.add_translation', ['id' => $dictionary->id]) }}" method="post" class="mt-5">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="original_word" placeholder="Original word" aria-label="Original word">
                    <input type="text" class="form-control" name="translation" placeholder="Translation" aria-label="Translation">
                    <button type="submit" class="btn btn-warning">Add</button>
                </div>
            </form>
        </div>
    </div>
@endsection
