@extends('base')

@section('content')
    <div class="row mt-5">
        <div class="col">
            <h3 class="text-center">Profile</h3>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col col-md-3">
            @include('profile/sidebar')
        </div>
        <div class="col offset-md-1 col-md-7">
                <h4>Create dictionary</h4>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('message'))
                <div class="alert alert-info">
                    {{ session()->get('message') }}
                </div>
            @endif
            <form action="{{ route('user.profile.create_dictionary') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputName" class="form-label">Name</label>
                    <input type="text" class="form-control" id="exampleInputName" name="name" required>
                </div>
                <div class="mb-3">
                    <label for="langDirection" class="form-label">Languages pair</label>
                    <select class="form-select" id="langDirection" aria-label="Languages pair" name="lang_direction">
                        <option value="en-ru" selected>EN-RU</option>
                        <option value="ru-en">RU-EN</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlDesc" class="form-label">Description</label>
                    <textarea class="form-control" id="exampleFormControlDesc" rows="3" name="description"></textarea>
                </div>
                <div class="mb-3">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="is_private" id="inlinePublic" value="0" checked>
                        <label class="form-check-label" for="inlinePublic">Public</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="is_private" id="inlinePrivate" value="1">
                        <label class="form-check-label" for="inlinePrivate">Private</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-warning">Create</button>
            </form>
        </div>
    </div>
@endsection
