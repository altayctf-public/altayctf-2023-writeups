@extends('base')

@section('content')
    <div class="row mt-5">
        <div class="col">
            <h3 class="text-center">Profile</h3>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col col-md-3">
            @include('profile/sidebar')
        </div>
        <div class="col col-md-9">
            <div class="row">
                <div class="col">
                    <h4 class="mb-3">My dictionaries</h4>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <a class="btn btn-warning" href="{{ route('user.profile.create_dictionary_form') }}">Create new</a>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4 mt-3">
                @forelse($dictionaries as $dictionary)
                <div class="col">
                    <div class="card h-100 bg-light-subtle border-light-subtle mb-3" style="max-width: 18rem;">
                        <div class="card-header">
                            {{ $dictionary->lang_direction }}
                            @if ($dictionary->is_private)
                                <span class="badge text-bg-secondary">Private</span>
                            @else
                                <span class="badge text-bg-primary">Public</span>
                            @endif
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $dictionary->name }}</h5>
                            <p class="card-text">{{ $dictionary->description }}</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('user.profile.edit_dictionary_form', ['id' => $dictionary->id]) }}" class="btn btn-warning" role="button" aria-disabled="true">Edit</a>
                            <form action="{{ route('user.profile.delete_dictionary', ['id' => $dictionary->id]) }}" method="post" style="display: inline">
                                @csrf
                                <input type="hidden" name="_method" value="delete"/>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
                @empty
                    No available dictionaries
                @endforelse
            </div>
        </div>
    </div>
@endsection
