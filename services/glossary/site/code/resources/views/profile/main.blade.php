@extends('base')

@section('content')
    <div class="row mt-5">
        <div class="col">
            <h3 class="text-center">Profile</h3>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col col-md-3">
            @include('profile/sidebar')
        </div>
        <div class="col offset-md-1 col-md-7">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('message'))
                <div class="alert alert-info">
                    {{ session()->get('message') }}
                </div>
            @endif
            <form action="{{ route('user.profile.update') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputUsername" class="form-label">Username</label>
                    <input type="text" class="form-control" id="exampleInputUsername" value="{{ $user->username }}" disabled>
                </div>
                <div class="mb-3">
                    <label for="exampleInputName" class="form-label">Your name</label>
                    <input type="text" class="form-control" id="exampleInputName" name="name" value="{{ $user->name }}" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail" class="form-label">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail" name="email"  value="{{ $user->email }}" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">New password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" aria-describedby="passwordHelp" name="password">
                    <div id="passwordHelp" class="form-text">Leave password field blank to not update.</div>
                </div>
                <button type="submit" class="btn btn-warning">Update profile</button>
            </form>
        </div>
    </div>
@endsection
