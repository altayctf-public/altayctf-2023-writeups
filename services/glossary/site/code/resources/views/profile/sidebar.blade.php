<div class="d-flex align-items-start">
    <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link sidebar-link mt-2 @if(\Illuminate\Support\Facades\Route::current()->getName() === 'user.profile') active @endif" href="{{ route('user.profile') }}">My profile</a>
        <a class="nav-link sidebar-link mt-2 @if(in_array(\Illuminate\Support\Facades\Route::current()->getName(), ['user.profile.dictionaries', 'user.profile.create_dictionary_form', 'user.profile.edit_dictionary_form'])) active @endif" href="{{ route('user.profile.dictionaries') }}">My dictionaries</a>
        <form action="{{ route('user.logout') }}" method="post">
            @csrf
            <button class="nav-link sidebar-link mt-2" type="submit">Logout</button>
        </form>
    </div>
</div>

@section('css')
    <style>
        .sidebar-link {
            color: #000 !important;
        }
        .sidebar-link.active {
            background-color: #ffc107 !important;
        }
        .sidebar-link:hover {
            background-color: #ffca2c !important;
        }
    </style>
@endsection
