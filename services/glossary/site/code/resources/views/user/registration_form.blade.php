@extends('base')

@section('content')
    <div class="row align-items-center" style="min-height: 100vh">
        <div class="col offset-md-4 col-md-4 p-4 bg-warning-subtle border border-warning border-opacity-25 rounded-2">
            <h3>Register</h3>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('message'))
                <div class="alert alert-info">
                    {{ session()->get('message') }}
                </div>
            @endif
            <form action="{{ route('user.do_register') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputName" class="form-label">Your name</label>
                    <input type="text" class="form-control" id="exampleInputName" name="name" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputUsername" class="form-label">Username</label>
                    <input type="text" class="form-control" id="exampleInputUsername" name="username" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail" class="form-label">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail" name="email" required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword" class="form-label">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword" aria-describedby="passwordHelp" name="password" required>
                    <div id="passwordHelp" class="form-text">We'll never share your credentials with anyone else.</div>
                </div>
                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
@endsection
