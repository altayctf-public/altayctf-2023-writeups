@extends('base')

@section('content')
    <div class="row align-items-center" style="min-height: 100vh">
        <div class="col offset-md-4 col-md-4 p-4 bg-warning-subtle border border-warning border-opacity-25 rounded-2">
            <h3>Login</h3>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('user.do_login') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputUsername" class="form-label">Username</label>
                    <input type="text" class="form-control" id="exampleInputUsername" name="username"  required>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" aria-describedby="passwordHelp" name="password" required>
                    <div id="passwordHelp" class="form-text">We'll never share your credentials with anyone else.</div>
                </div>
                <div class="mb-3">
                    Don't have an account? Please <a href="{{ route('user.registration_form') }}" class="link-warning">register</a>
                </div>
                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
@endsection
