package sqlstore

import (
	"database/sql"
	"glossary.altayctf.ru/internal/app/store"
)

type Store struct {
	db                   *sql.DB
	dictionaryRepository *DictionaryRepository
}

func New(db *sql.DB) *Store {
	return &Store{
		db: db,
	}
}

func (s *Store) Dictionary() store.DictionaryRepository {
	if s.dictionaryRepository != nil {
		return s.dictionaryRepository
	}

	s.dictionaryRepository = &DictionaryRepository{
		store: s,
	}

	return s.dictionaryRepository
}
