package sqlstore

import (
	"fmt"
	"glossary.altayctf.ru/internal/app/models"
	"glossary.altayctf.ru/internal/app/store"
	"go.uber.org/zap"
)
import "database/sql"

type DictionaryRepository struct {
	store *Store
}

func (r *DictionaryRepository) FindLastTranslation(word string, langPair string) (*models.Translation, error) {
	t := &models.Translation{}
	query := fmt.Sprintf("SELECT t.id, t.original_word, t.translation FROM `translations` AS t JOIN dictionaries d ON d.id = t.dictionary_id WHERE d.lang_direction = '%s' AND t.original_word = '%s' AND d.is_private = 0 ORDER BY t.id DESC", langPair, word)
	if err := r.store.db.QueryRow(query).Scan(
		&t.ID,
		&t.OriginalWord,
		&t.Translation,
	); err != nil {
		zap.L().Debug("last failed: " + err.Error())
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return t, nil
}

func (r *DictionaryRepository) FindAllTranslations(word string, langPair string) (*[]models.Translation, error) {
	query := "SELECT t.id, t.original_word, t.translation FROM `translations` AS t JOIN dictionaries d ON d.id = t.dictionary_id WHERE d.lang_direction = ? AND t.original_word = ? AND d.is_private = 0 ORDER BY t.id DESC"
	zap.L().Debug(query)
	rows, err := r.store.db.Query(query, langPair, word)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}
	defer rows.Close()

	var t []models.Translation

	for rows.Next() {
		var translation models.Translation
		if err := rows.Scan(
			&translation.ID,
			&translation.OriginalWord,
			&translation.Translation,
		); err != nil {
			return nil, err
		}

		t = append(t, translation)
	}

	return &t, nil
}
