package store

import (
	"glossary.altayctf.ru/internal/app/models"
)

type DictionaryRepository interface {
	FindLastTranslation(word string, langPair string) (*models.Translation, error)
	FindAllTranslations(word string, langPair string) (*[]models.Translation, error)
}
