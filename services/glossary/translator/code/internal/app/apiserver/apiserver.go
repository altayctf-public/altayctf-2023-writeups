package apiserver

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"glossary.altayctf.ru/internal/app/configs"
	"glossary.altayctf.ru/internal/app/store/sqlstore"
	"net"
)

func Start() error {
	config, err := configs.GetAppConfig()
	if err != nil {
		return err
	}

	db, err := newDB(config.DatabaseUrl)
	if err != nil {
		return err
	}

	defer db.Close()
	store := sqlstore.New(db)

	srv := newServer(store)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%v", config.BindPort))
	if err != nil {
		return err
	}

	if err := srv.grpc.Serve(lis); err != nil {
		return err
	}

	return nil
}

func newDB(dbURL string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dbURL)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
