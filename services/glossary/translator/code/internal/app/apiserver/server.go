package apiserver

import (
	"context"
	"fmt"
	pb "glossary.altayctf.ru/internal/app/services/proto"
	"glossary.altayctf.ru/internal/app/store"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"strings"
)

type server struct {
	grpc  *grpc.Server
	store store.Store
}

type translatorServiceServer struct {
	store store.Store
	pb.UnimplementedTranslateServiceServer
}

func newServer(store store.Store) *server {
	s := &server{
		grpc:  grpc.NewServer(),
		store: store,
	}

	pb.RegisterTranslateServiceServer(s.grpc, &translatorServiceServer{
		store: s.store,
	})

	return s
}

func (s *translatorServiceServer) TranslateText(ctx context.Context, query *pb.Query) (*pb.TranslationResult, error) {
	zap.L().Debug(fmt.Sprintf("Source text: %v", query.Text))

	var lp string
	if query.LangPair == pb.LangPair_LANGPAIR_RU_EN {
		lp = "ru-en"
	} else {
		lp = "en-ru"
	}

	originalWords := strings.Split(query.Text, " ")
	var translatedText string
	var translationVariants []*pb.Synonyms
	var translationPairs []string

	for _, word := range originalWords {
		allTranslations, err := s.store.Dictionary().FindAllTranslations(word, lp)
		if err == nil {
			var synonyms []string
			for _, t := range *allTranslations {
				synonym := t.Translation
				synonyms = append(synonyms, synonym)
			}
			wordSynonyms := &pb.Synonyms{
				SourceWord: word,
				Synonyms:   synonyms,
			}

			translationVariants = append(translationVariants, wordSynonyms)
		} else if err == store.ErrRecordNotFound {
			var synonyms []string
			synonyms = append(synonyms, "no available translations")
			wordSynonyms := &pb.Synonyms{
				SourceWord: word,
				Synonyms:   synonyms,
			}

			translationVariants = append(translationVariants, wordSynonyms)
		} else {
			return nil, err
		}

		translation, err := s.store.Dictionary().FindLastTranslation(word, lp)
		if err == nil {
			translationPairs = append(translationPairs, word)
			translationPairs = append(translationPairs, translation.Translation)
		} else if err == store.ErrRecordNotFound {
			// nothing to replace, leave the original text
		} else {
			return nil, err
		}
	}

	replacer := strings.NewReplacer(translationPairs...)
	translatedText = replacer.Replace(query.Text)

	return &pb.TranslationResult{
		SourceText:      query.Text,
		TranslatedWords: translationVariants,
		TranslatedText:  translatedText,
	}, nil
}
