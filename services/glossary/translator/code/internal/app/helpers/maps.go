package helpers

import "sort"

func MapToArray(a map[string]string) []string {
	r := make([]string, 2*len(a))

	i := 0
	keys := make([]string, 0, len(a))

	for k, _ := range a {
		keys = append(keys, k)
	}

	// sort by key length descending
	sort.SliceStable(keys, func(i, j int) bool {
		return len(keys[i]) > len(keys[j])
	})

	for _, k := range keys {
		r[i*2] = k
		r[i*2+1] = a[k]
		i++
	}

	return r
}

func MergeStringMaps(firstMap map[string]string, secondMap map[string]string) map[string]string {
	resultMap := make(map[string]string)

	for k, v := range firstMap {
		resultMap[k] = v
	}

	for k, v := range secondMap {
		resultMap[k] = v
	}

	return resultMap
}
