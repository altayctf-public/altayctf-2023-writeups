package helpers

import (
	"golang.org/x/text/encoding/unicode"
	"regexp"
	"strings"
)

func Normalize(text string) (string, error) {
	processedText, err := utf8to16(text)
	if err != nil {
		return "", err
	}

	textLength := len(processedText)
	for i := 0; i < textLength; i += 2 {
		h := processedText[i]
		l := processedText[i+1]
		if h == 255 && l >= 1 && l <= 94 {
			textBytes := []byte(processedText)
			textBytes[i] = 0
			textBytes[i+1] = l + 32
			processedText = string(textBytes)
		} else if h == 255 && l == 229 {
			textBytes := []byte(processedText)
			textBytes[i] = 0
			textBytes[i+1] = 165
			processedText = string(textBytes)
		} else if h == 34 && l == 18 {
			textBytes := []byte(processedText)
			textBytes[i] = 0
			textBytes[i+1] = 45
			processedText = string(textBytes)
		} else if h == 48 && l == 0 {
			textBytes := []byte(processedText)
			textBytes[i] = 0
			textBytes[i+1] = 32
			processedText = string(textBytes)
		}
	}

	processedText, err = utf16to8(processedText)
	if err != nil {
		return "", err
	}

	return processedText, nil
}

func Filter(text string, char string) (string, error) {
	processedText, err := utf8to16(text)
	if err != nil {
		return "", err
	}
	textLength := len(processedText)
	for i := 0; i < textLength; i += 2 {
		h := processedText[i]
		l := processedText[i+1]
		if !((h == 0 && (l >= 32 && l <= 126 || l == 10 || l == 13 || l == 165 || l == 9 || l == 11)) || (h == 4 && l >= 1 && l <= 249)) {
			textBytes := []byte(processedText)
			textBytes[i] = 0
			textBytes[i+1] = 0
			processedText = string(textBytes)
		}
	}
	processedText, err = utf16to8(processedText)
	re := regexp.MustCompile("\\x{0000}+")
	processedText = re.ReplaceAllString(processedText, " "+strings.Repeat(char, 3)+" ")

	return processedText, nil
}

func utf16to8(text string) (string, error) {
	utf16be := unicode.UTF16(unicode.BigEndian, unicode.IgnoreBOM)
	utfDecoder := utf16be.NewDecoder()
	utf16beDecodedText, err := utfDecoder.String(text)

	return utf16beDecodedText, err
}

func utf8to16(text string) (string, error) {
	utf16be := unicode.UTF16(unicode.BigEndian, unicode.IgnoreBOM)
	utfEncoder := utf16be.NewEncoder()
	utf16beEncodedText, err := utfEncoder.String(text)

	return utf16beEncodedText, err
}
