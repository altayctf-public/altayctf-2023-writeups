package helpers

import (
	"fmt"
	"go.uber.org/zap"
	"time"
)

func TimeTrack(start time.Time, name string) {
	end := time.Now()
	elapsed := end.Sub(start)
	zap.L().Debug(fmt.Sprintf("%s took %s", name, elapsed))
}
