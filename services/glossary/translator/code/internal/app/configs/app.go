package configs

import (
	"github.com/BurntSushi/toml"
	"os"
)

const configPath = "configs/app.toml"

var appConfigInstance *AppConfig

type AppConfig struct {
	BindAddr    string
	BindPort    string
	DatabaseUrl string
	LogLevel    string `toml:"log_level"`
}

func GetAppConfig() (*AppConfig, error) {
	if appConfigInstance == nil {

		appConfigInstance = &AppConfig{
			BindAddr:    os.Getenv("BIND_ADDR"),
			BindPort:    os.Getenv("BIND_PORT"),
			DatabaseUrl: os.Getenv("DB_URL"),
		}

		_, err := toml.DecodeFile(configPath, appConfigInstance)
		if err != nil {
			return nil, err
		}
	}

	return appConfigInstance, nil
}
