package models

type Translation struct {
	ID           int
	OriginalWord string
	Translation  string
}
