package main

import (
	"github.com/joho/godotenv"
	"glossary.altayctf.ru/internal/app/apiserver"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

func main() {
	configureLogger()

	if err := godotenv.Load(); err != nil {
		zap.L().Fatal("Error loading .env file")
		os.Exit(1)
	}

	zap.L().Info("Starting server instance")

	if err := apiserver.Start(); err != nil {
		zap.L().Fatal(err.Error())
		os.Exit(1)
	}
}

func configureLogger() {
	encoder := getEncoder()
	core := zapcore.NewCore(encoder, os.Stdout, zapcore.DebugLevel)
	logger := zap.New(core, zap.AddCaller())
	zap.ReplaceGlobals(logger)

	zap.L().Info("Logger was configured successfully")
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewJSONEncoder(encoderConfig)
}
