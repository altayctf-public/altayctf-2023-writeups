# Glossary - сервис перевода текста

## Технологический стек

1. Site - PHP + Laravel 
2. Translator microservice - Go
3. Веб-сервер nginx
3. СУБД Mariadb
4. Протокол взаимодействия между внутренними сервисами - gRPC + protobuf

## Описание функционала

1. Главная страница - отображение последних N-переводов пользователей из публичных словарей
2. Регистрация/Авторизация
3. Профиль:
    3.1 Редактирование данных пользователя (имя, email, задание нового пароля)
    3.2 Отображения словарей пользвателей (редактирование с добавлением переводов в соварь, удаление словаря)
    3.3 Создание нового словаря с возможностью сделать его приватным или публичным.
    3.4 Выход из профиля
4. Поиск перевода текста - выводится в 1-й таблице исходный текст с переводом, во 2-й - для каждого слова из текста список возможных вариантов перевода из публичных словарей.

## Уязвимость №1 - IDOR

Простая уязвимость в форме редактирования словаря в интерфейсе профиля пользователя. Перечисление id словаря в URL вида ```/profile/dictionaries/<id>``` позволяет получить доступ к словарям других пользователей, в том числе приватным, где собственно, могут храниться флаги.

Проблемный участок кода находится в методе `dictionaryEditForm` класса `ProfileController`:
```
$dict = Dictionary::find($id);

if ($dict === null) {
    return back();
}

if (Gate::allows('edit-dictionary', $dict)) {
    return view('profile.edit_dictionary_form', ['dictionary' => $dict]);
} else {
    abort(403);
}
```

Используется встроенный в Laravel механизм Gate для разрешения/запрета редактирования словаря, найденного предварительно в БД по переданному в URL id. Политика доступа `edit-dictionary` в рамках этого Gate определена в классе `App\Providers\AuthServiceProvider`, в методе boot:

```
Gate::define('edit-dictionary', function (User $user, Dictionary $dictionary) {
    return (($user->id !== null) || ($user->id === $dictionary->user_id));
});
```

Как видим, здесь разработчиком была допущена логичекая ошибка, и поскольку пользователь авторизован, модель пользователя имеет not null-свойство id и поэтому логическое условие будет возвращать TRUE. Таким образом, любой авторизованный пользователь имеет доступ к форме редактирования любого словаря, зная только его id.

**Приведем 2 варианта фикса**:
1. Исправляем логическое условие || на && в определении политики доступа:
    ```
    Gate::define('edit-dictionary', function (User $user, Dictionary $dictionary) {
        return (($user->id !== null) && ($user->id === $dictionary->user_id));
    });
    ```
    В таком случае, при обращении к id словаря, к которому пользователь не имеет доступ, будет возвращаться страница с кодом 403.
    
2. Исходя из кода приложения мы знаем, что сущности User, Dictionary связаны отношением "один ко многим". Таким образом мы можем получить словарь по его id, сделав запрос только к словарям текущего пользователя. Кроме того, теперь можно не использовать механизм политик доступа:
    ```
    /** @var User $user */
    $user = Auth::user();

    /** @var Dictionary $dict */
    $dict = $user->dictionaries()->find($id);

    if ($dict === null) {
        return redirect()->route('user.profile.dictionaries');
    }

    return view('profile.edit_dictionary_form', ['dictionary' => $dict]);
    ```

## Уязвимость №2 - SQL injection

При вводе и отправке какого-либо текста для перевода на стороне сайта происходит запрос на микросервис **translator**, реализованный на Go. Протокол передачи gRPC, передаваемые данные сериализуются с помощью protobuf. Даннный микросервис для исходного текста возвращает переведенный текст, а также список вариантов перевода для каждого слова в тексте.

Уязвимый код находится в методе `FindLastTranslation` структуры `DictionaryRepository`:
```
t := &models.Translation{}
query := fmt.Sprintf("SELECT t.id, t.original_word, t.translation FROM `translations` AS t JOIN dictionaries d ON d.id = t.dictionary_id WHERE d.lang_direction = '%s' AND t.original_word = '%s' AND d.is_private = 0 ORDER BY t.id DESC", langPair, word)
if err := r.store.db.QueryRow(query).Scan(
	&t.ID,
	&t.OriginalWord,
	&t.Translation,
); err != nil {
	zap.L().Debug("last failed: " + err.Error())
	if err == sql.ErrNoRows {
		return nil, store.ErrRecordNotFound
	}

	return nil, err
}

return t, nil
```

Здесь, как мы видим, при формировании запроса параметры передаются напрямую с помощью метода форматирования строк, т.е. нет использования механизма подготовленных выражений (prepared statements). Таким образом, возможна инъекция sql-кода в запрос. Поскольку исходный текст предварительно разбивается на слова по символу пробела в gRPC-обработчике запроса `TranslateText` (internal\app\apiserver\server.go):
```
originalWords := strings.Split(query.Text, " ")
```

то для успешной эксплуатации нужно обойти символ пробела. Как вариантов, использовать символ комментария. Один из рабочих вариантов возможной инъекции на стороне сайта:
```
/search?text=val1%27/**/OR/**/1=1/**/ORDER/**/BY/**/id/**/DESC/**/LIMIT/**/0,1/**/%23&lang_direction=0
```
Перебирая первый параметр оператора LIMIT, можем вытащить флаги, т.к. мы обошли проверку на приватный словарь.

**Приведем также 2 варианта фикса**:
1. Фильтрация спецсимволов как на стороне сайта перед отправкой запроса по gRPC, так и на стороне микросервиса на Go.
2. Использование подготовленных выражений при запросе к БД на стороне микросервиса translator:
    ```
    query := "SELECT t.id, t.original_word, t.translation FROM `translations` AS t JOIN dictionaries d ON d.id = t.dictionary_id WHERE d.lang_direction = ? AND t.original_word = ? AND d.is_private = 0 ORDER BY t.id DESC"
	rows, err := r.store.db.QueryRow(query, langPair, word)
    ```
