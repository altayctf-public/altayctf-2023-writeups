﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public double Balance { get; set; }
        public string SecretQuestion { get; set; }
        public string SecretAnsver { get; set; }
        public string SessionToken { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
