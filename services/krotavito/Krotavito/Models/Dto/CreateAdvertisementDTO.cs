﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class CreateAdvertisementDTO
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int UserId { get; set; }
        public string SessionToken { get; set; }
    }
}
