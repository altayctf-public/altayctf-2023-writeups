﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class LoginResponseDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string SessionToken { get; set; }
    }
}
