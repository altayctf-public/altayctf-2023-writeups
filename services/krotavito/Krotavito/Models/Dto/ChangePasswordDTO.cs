﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class ChangePasswordDTO
    {
        public int Id { get; set; }
        public string NewPassword { get; set; }
        public string SessionToken { get; set; }
    }
}
