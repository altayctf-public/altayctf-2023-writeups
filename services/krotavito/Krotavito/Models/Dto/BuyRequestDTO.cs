﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class BuyRequestDTO
    {
        public int AdvertisementId { get; set; }
        public int UserId { get; set; }
        public string SessionToken { get; set; }
    }
}
