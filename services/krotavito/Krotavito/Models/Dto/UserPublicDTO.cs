﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class UserPublicDTO
    {
        public string Username { get; set; }
        public DateTime DateRegistered { get; set; }
    }
}
