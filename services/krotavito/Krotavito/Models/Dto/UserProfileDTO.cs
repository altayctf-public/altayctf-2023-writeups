﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class UserProfileDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public double Balance { get; set; }
        public string SecretQuestion { get; set; }
        public string SecretAnsver { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
