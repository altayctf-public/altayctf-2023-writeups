﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class RecoverPasswordRequestDTO
    {
        public int id { get; set; }
        public string SecretQuestion { get; set; }
        public string SecretAnsver { get; set; }
        public string NewPassword { get; set; }
    }
}
