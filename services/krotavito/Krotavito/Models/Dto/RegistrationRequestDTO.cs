﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class RegistrationRequestDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string SecretQuestion { get; set; }
        public string SecretAnsver { get; set; }
    }
}
