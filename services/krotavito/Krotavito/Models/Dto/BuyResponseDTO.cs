﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class BuyResponseDTO
    {
        public string Result { get; set; }
        public double Price { get; set; }
        public double CurrentBalance { get; set; }
    }
}
