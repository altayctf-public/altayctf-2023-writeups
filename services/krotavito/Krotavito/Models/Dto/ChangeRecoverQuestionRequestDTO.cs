﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class ChangeRecoverQuestionRequestDTO
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Ansver { get; set; }
        public string SessionToken { get; set; }
    }
}
