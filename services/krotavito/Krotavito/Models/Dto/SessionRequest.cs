﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Models.Dto
{
    public class SessionRequest
    {
        public int UserId { get; set; }
        public string SessionToken { get; set; }
    }
}
