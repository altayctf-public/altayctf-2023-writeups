﻿using Krotavito.Data;
using Krotavito.Models;
using Krotavito.Models.Dto;
using Krotavito.Functools;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        public UserController(ApplicationDbContext db)
        {
            _db = db;
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpPost("Register")]
        public IActionResult Register([FromBody] RegistrationRequestDTO registrationRequestDTO)
        {
            if (registrationRequestDTO == null)
                return BadRequest(registrationRequestDTO);

            User user = new User()
            {
                Username = registrationRequestDTO.Username,
                PasswordHash = Crypto.sha256(registrationRequestDTO.Password),
                Balance = 1000.0,
                SecretAnsver = registrationRequestDTO.SecretAnsver,
                SecretQuestion = registrationRequestDTO.SecretQuestion,
            };

            try
            {
                _db.Users.Add(user);
                _db.SaveChanges();
                return Ok(registrationRequestDTO);
            }
            catch
            {
                return BadRequest();
            }
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [HttpPost("Login")]
        public ActionResult<LoginResponseDTO> Login([FromBody] LoginRequestDTO loginRequestDTO)
        {

            if (loginRequestDTO == null)
                return BadRequest(loginRequestDTO);

            User user = _db.Users.FirstOrDefault(u => u.Username == loginRequestDTO.Username && u.PasswordHash == Crypto.sha256(loginRequestDTO.Password));
            if (user == null)
                return Forbid();

            string sesionToken = Crypto.generateToken(user.Id, user.Username);
            user.SessionToken = sesionToken;
            _db.Users.Update(user);
            _db.SaveChanges();

            LoginResponseDTO response = new LoginResponseDTO();
            response.Id = user.Id;
            response.SessionToken = sesionToken;
            response.Username = user.Username;

            return Ok(response);
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [HttpPost("RecoverPassword")]
        public IActionResult RecoverPassword([FromBody] RecoverPasswordRequestDTO recoverRequestDTO)
        {
            if (recoverRequestDTO == null)
                return BadRequest(recoverRequestDTO);

            User user = _db.Users.FirstOrDefault(q => q.SecretQuestion == recoverRequestDTO.SecretQuestion && q.SecretAnsver == recoverRequestDTO.SecretAnsver && q.Id == recoverRequestDTO.id);
            //User user = _db.Users.FirstOrDefault(q => q.SecretQuestion == recoverRequestDTO.SecretQuestion);
            if (user == null)
                return Forbid();
            user.PasswordHash = Crypto.sha256(recoverRequestDTO.NewPassword);
            _db.Users.Update(user);
            _db.SaveChanges();
            return Ok();
        }

        [HttpPost("ChangeRecoverQuestion")]
        [ProducesResponseType(200)]
        public IActionResult ChangeRecoverQuestion([FromBody] ChangeRecoverQuestionRequestDTO recoverRequestDTO)
        {
            if (recoverRequestDTO == null)
                return BadRequest();
            var currentUser = _db.Users.FirstOrDefault(u => u.Id == recoverRequestDTO.Id);
            if (currentUser == null)
                return Forbid();

            if (recoverRequestDTO.Question != null)
                currentUser.SecretQuestion = recoverRequestDTO.Question;
            if (recoverRequestDTO.Ansver != null)
                currentUser.SecretAnsver = recoverRequestDTO.Ansver;

            _db.Users.Update(currentUser);
            _db.SaveChanges();

            return Ok();
        }

        [HttpPost("ChangePassword")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult ChangePassword([FromBody] ChangePasswordDTO changePasswordDTO)
        {
            if (changePasswordDTO == null)
                return BadRequest();
            var currentUser = _db.Users.FirstOrDefault(u => u.Id == changePasswordDTO.Id);
            if (currentUser == null)
                return Forbid();

            if (changePasswordDTO.NewPassword != null)
                currentUser.PasswordHash = Crypto.sha256(changePasswordDTO.NewPassword);
            else
                return BadRequest();

            _db.Users.Update(currentUser);
            _db.SaveChanges();

            return Ok();
        }

        [ProducesResponseType(200)]
        [HttpPost("All")]
        public ActionResult<UserPublicDTO> GetAllUsers()
        {
            List<User> users = _db.Users.ToList();
            List<UserPublicDTO> usersPublicDTO = new List<UserPublicDTO>();
            foreach (User user in users)
            {
                string username = user.Username;
                DateTime dateRegistered = user.DateCreated;
                UserPublicDTO userPublicDTO = new UserPublicDTO()
                {
                    Username = user.Username,
                    DateRegistered = user.DateCreated

                };
                usersPublicDTO.Add(userPublicDTO);
            }
            return Ok(usersPublicDTO.TakeLast(200));
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        [HttpPost("Profile")]
        public ActionResult<UserProfileDTO> GetMyProfile([FromBody] SessionRequest session)
        {
            if (session == null)
                return Forbid();
            var currentUser = _db.Users.FirstOrDefault(u => u.SessionToken == session.SessionToken);
            if (currentUser == null)
                return Forbid();

            UserProfileDTO profile = new UserProfileDTO()
            {
                Id = currentUser.Id,
                Username = currentUser.Username,
                Balance = currentUser.Balance,
                SecretQuestion = currentUser.SecretQuestion,
                SecretAnsver = currentUser.SecretAnsver
            };

            return Ok(profile);
        }

    }
}
