﻿using Krotavito.Data;
using Krotavito.Models;
using Krotavito.Models.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Controllers
{
    [Route("api/advertisement")]
    [ApiController]
    public class AdvertisementController : ControllerBase
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        private readonly ApplicationDbContext _db;
        public AdvertisementController(ApplicationDbContext db)
        {
            _db = db;
        }


        [HttpGet]
        public ActionResult<IEnumerable<CreateAdvertisementDTO>> GetAdvertisements()
        {
            return Ok(_db.Advertisemens.ToList().TakeLast(200));
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult<CreateAdvertisementDTO> GetAdvertisement(int id)
        {
            var advertisement = _db.Advertisemens.FirstOrDefault(u => u.Id == id);

            if (advertisement == null)
                return NotFound();

            return Ok(advertisement);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public ActionResult<CreateAdvertisementDTO> CreateAdvertisement([FromBody]CreateAdvertisementDTO createAdvertisementDTO)
        {
            if (createAdvertisementDTO == null)
                return BadRequest(createAdvertisementDTO);
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (createAdvertisementDTO.SessionToken == null)
                return Forbid();
            var currentUser = _db.Users.FirstOrDefault(u => u.SessionToken == createAdvertisementDTO.SessionToken);
            if (currentUser == null)
                return Forbid();
            //advertisementDTO.Id = AdvertisementStore.advertisementList.OrderByDescending(u => u.Id).FirstOrDefault().Id + 1;
            Advertisement advertisement = new Advertisement()
            {
                Title = createAdvertisementDTO.Title,
                Description = createAdvertisementDTO.Description,
                Price = createAdvertisementDTO.Price,
                OwnerId = currentUser.Id,
                DateCreated = DateTime.Now
            };
            _db.Advertisemens.Add(advertisement);
            _db.SaveChanges();

            return Ok(createAdvertisementDTO);
        }

        [HttpDelete("{id:int}", Name = "DeleteAdvertisement")]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult DeleteAdvertisement(int id, [FromBody] SessionRequest session)
        {
            if (session == null)
                return Forbid();
            var currentUser = _db.Users.FirstOrDefault(u => u.SessionToken == session.SessionToken);
            if (currentUser == null)
                return Forbid();
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var advertisement = _db.Advertisemens.FirstOrDefault(u => u.Id == id);
            if (advertisement == null)
                return NotFound();

            _db.Advertisemens.Remove(advertisement);
            _db.SaveChanges();

            return Ok();
        }

        [HttpPut("{id:int}", Name = "UpdateAdvertisement")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult UpdateAdvertisement(int id, [FromBody]CreateAdvertisementDTO advertisementDTO)
        {
            if (advertisementDTO == null)
                return Forbid();
            var currentUser = _db.Users.FirstOrDefault(u => u.SessionToken == advertisementDTO.SessionToken);
            if (currentUser == null)
                return Forbid();
            if (advertisementDTO == null)
                return BadRequest();

            var advertisement = _db.Advertisemens.FirstOrDefault(u => u.Id == id);
            if(advertisement == null || advertisement.OwnerId != currentUser.Id)
                return BadRequest();

            advertisement.Title = advertisementDTO.Title;
            advertisement.Price = advertisementDTO.Price;
            advertisement.Description = advertisementDTO.Description;

            _db.Advertisemens.Update(advertisement);
            _db.SaveChanges();

            return Ok();
        }

        [HttpPost("Buy")]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public ActionResult<BuyResponseDTO> BuyAdvertisement([FromBody] BuyRequestDTO buyDTO)
        {
            if (buyDTO == null)
                return Forbid();
            var currentUser = _db.Users.FirstOrDefault(u => u.SessionToken == buyDTO.SessionToken);
            if (currentUser == null)
                return Forbid();
            var advertisement = _db.Advertisemens.FirstOrDefault(u => u.Id == buyDTO.AdvertisementId);
            if (advertisement == null)
                return NotFound();

            double tryBuy = currentUser.Balance - advertisement.Price;
            var response = new BuyResponseDTO();
            if (tryBuy < 0)
            {
                response.Result = "Insufficient funds";
                response.Price = advertisement.Price;
                response.CurrentBalance = currentUser.Balance;
                return Ok(response);
            }
            else
            {
                currentUser.Balance = tryBuy;
                advertisement.OwnerId = currentUser.Id;
                _db.Users.Update(currentUser);
                _db.Advertisemens.Update(advertisement);
                _db.SaveChanges();

                response.Result = "The payment was completed";
                response.Price = advertisement.Price;
                response.CurrentBalance = tryBuy;
                return Ok(response);
            }
        }
    }
}
