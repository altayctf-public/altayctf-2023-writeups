﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Krotavito.Functools
{
    public class Crypto
    {
        //https://stackoverflow.com/questions/12416249/hashing-a-string-with-sha256
        public static string sha256(string randomString)
        {
            var crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }

        public static string generateToken(int id, string Username)
        {
            string token = "__" + sha256(id.ToString() + Username);
            return token;

        }
    }
}
