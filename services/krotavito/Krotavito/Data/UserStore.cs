﻿using Krotavito.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Data
{
    public class UserStore
    {
        public static List<UserProfileDTO> userList = new List<UserProfileDTO>
        {
            new UserProfileDTO{Id=1, Username="Babuin",  SecretQuestion="Good Babuin!", SecretAnsver="Good Babuin!", Balance=10000.0},
            new UserProfileDTO{Id=2, Username="Babaika",  SecretQuestion="Babaika!", SecretAnsver="Good Babuin!", Balance=15000.0}
        };
    }
}
