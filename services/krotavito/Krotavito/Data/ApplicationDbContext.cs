﻿using Krotavito.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Krotavito.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Advertisement> Advertisemens { get; set; }
        public DbSet<User> Users { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Advertisement>().HasData(
        //        new Advertisement()
        //        {
        //            Id=1,
        //            Title= "First Advertisement",
        //            Description="Desc",
        //            BonusBySaller="Bonus!",
        //            Price=10000.0,
        //            OwnerId=1,
        //            DateCreated = DateTime.Now
        //        }
        //        );
        //}
    }
}
